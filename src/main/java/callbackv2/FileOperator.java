package callbackv2;

import callbackv2.callbacks.ICallback;
import callbackv2.callbacks.WritingToFileCallBack;
import callbackv2.tasks.RecursiveFileSearchTask;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileOperator implements ICallback {
    private ExecutorService threads = Executors.newFixedThreadPool(3);

    public void searchForFile(String path,String fileName){
        threads.submit(new RecursiveFileSearchTask(this,path,fileName));
    }
    public void writeToFileFilePaths(String path, String fileName){
        threads.submit(new RecursiveFileSearchTask(new WritingToFileCallBack(),path,fileName));
    }

    @Override
    public void finishedTask() {
        System.out.println("Finished task");
    }

    @Override
    public void finishedTask(List<String> results) {
        System.out.println("File path: " + results.toString());
        System.out.println("Finished task");
    }
}
