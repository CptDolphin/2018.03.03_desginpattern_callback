package callbackv2.factories;

import callbackv2.FileOperator;
import callbackv2.callbacks.ICallback;

public class CallbackFactory {
    public ICallback getCallback(String option,String path, String fileName){
        FileOperator operator = new FileOperator();
        switch(option){
            case"search":operator.searchForFile(path,fileName);
            case"write":operator.writeToFileFilePaths(path,fileName);
            default:
                throw new RuntimeException("Can't create callback based on type " + option);
        }
    }
}
