Create an application that performs recursive search of the file with the given name. The search is to be done in a separate thread.
After finding the files, callback deals with listing all files.
Create a second callback which saves the paths of these files to another file
Create a factory callback (the two mentioned above)
Test the application from maina. Create a fasade (the FilesFinder class) which has the find method and takes the callback as its parameter. handle the case when the callback is not given (it is equal to null).