package callbackv2.callbacks;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.List;

public class WritingToFileCallBack implements ICallback {
    @Override
    public void finishedTask() {
    }

    @Override
    public void finishedTask(List<String> results) {
        try(PrintWriter writer = new PrintWriter("paths.txt")) {
            for(String str: results){
                writer.println(str);

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
