package callbackv2.callbacks;

import java.util.List;

public interface ICallback {
    void finishedTask();
    void finishedTask(List<String> results);
}
