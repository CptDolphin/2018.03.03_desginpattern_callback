package callbackv2.tasks;

import callbackv2.callbacks.ICallback;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class RecursiveFileSearchTask implements Runnable {
    private ICallback callback;
    private String path;
    private String filename;
    private static List<String> results;

    public RecursiveFileSearchTask(ICallback callback, String path, String filename) {
        this.callback = callback;
        this.path = path;
        this.filename = filename;
        results = new ArrayList<>();
    }

    @Override
    public void run() {
        recursiveFileSearch(path, filename);
        if (callback != null) {
            callback.finishedTask(results);
        }
    }

    private static void recursiveFileSearch(String path, String filename) {
        File file = new File(path);
        if (file.exists() && file.isDirectory() && file.canRead()) {
            List<File> fileList = Arrays.asList(file.listFiles());
            for (File f : fileList) {
                if (f.getName().startsWith(filename)) {
                    results.add(f.getAbsolutePath());
                }else if (f.isDirectory()) {
                    recursiveFileSearch(f.getAbsolutePath(), filename);
                }
            }
        }
    }
}
