package callbackv2;

import callbackv2.factories.CallbackFactory;

public class Main {

    public static final String PATH = "c:/users/user/ideaprojects";
    public static final String DESTINATION = "pom.xml";

    public static void main(String[] args) {
        CallbackFactory operator = new CallbackFactory();
//        operator.getCallback("write",PATH,DESTINATION);
        operator.getCallback("search", PATH, DESTINATION);
    }
}
