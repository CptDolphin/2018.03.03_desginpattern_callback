package callback.tasks;

import callback.callbacks.ICallback;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ReadingNumbersFromFileTask implements Runnable {
    private ICallback callback;

    public ReadingNumbersFromFileTask(ICallback callback) {
        this.callback = callback;
    }

    public void run() {
        List<Integer> numbers = new LinkedList<>();
        try (Scanner scanner = new Scanner(new File("numbers.txt"))) {
            while (scanner.hasNextInt()) {
                numbers.add(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if (callback != null) {
            callback.finishedTask(numbers);
        }
    }
}
