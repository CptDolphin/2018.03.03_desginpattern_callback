package callback.tasks;

import callback.callbacks.ICallback;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class GeneratorNumberToFileTask implements Runnable {
    private int howMuch;
    private ICallback callback;

    public GeneratorNumberToFileTask(int howMuch, ICallback callback) {
        this.howMuch = howMuch;
        this.callback = callback;
    }

    public void run() {
        Random random = new Random();
        List<Integer> numbers = new LinkedList<>();
        for (int i = 0; i < howMuch; i++) {
            numbers.add(random.nextInt());
        }

        try (PrintWriter writer = new PrintWriter("numbers.txt")) {
            while (!numbers.isEmpty()) {
                writer.print(numbers.remove(0));
                writer.print(" ");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        if(callback!=null){
            callback.finishedTask();
        }
    }
}
