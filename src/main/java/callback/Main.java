package callback;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        FileOperator operator = new FileOperator();
        boolean isWorking = true;
        while (isWorking) {
            String command = scanner.nextLine();
            if(command.startsWith("gen")){
                operator.generateNumberToFile(Integer.parseInt(command.split(" ")[1]));
            }else if(command.startsWith("read")){
                operator.readNumbersFromFile();
            }else if(command.startsWith("sum")){
                operator.countSumOfAllNumbers();
            }else if(command.startsWith("mean")){
                operator.readNumbersGiveMean();
            }else if(command.startsWith("sort")){
                operator.sortNumbers();
            }else if(command.startsWith("quit")){
                System.exit(0);
            }
        }
    }
}
