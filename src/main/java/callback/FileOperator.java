package callback;

import callback.callbacks.ICallback;
import callback.callbacks.MeanCallBack;
import callback.callbacks.SortCallBack;
import callback.callbacks.SumCallBack;
import callback.tasks.GeneratorNumberToFileTask;
import callback.tasks.ReadingNumbersFromFileTask;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileOperator implements ICallback {
    private int startedTasks = 0;
    private int finishedTasks = 0;
    private ExecutorService threads = Executors.newFixedThreadPool(3);

    public void generateNumberToFile(int howMuch) {
        threads.submit(new GeneratorNumberToFileTask(howMuch,this));
        System.out.printf("Started tasks: %d\n",++startedTasks);
    }

    public void readNumbersFromFile(){
        threads.submit(new ReadingNumbersFromFileTask(this));
        System.out.printf("Started tasks: %d\n",++startedTasks);
    }

    public void countSumOfAllNumbers(){
        threads.submit(new ReadingNumbersFromFileTask(new SumCallBack()));
        System.out.printf("Started tasks: %d\n",++startedTasks);
    }
    public void readNumbersGiveMean() {
        threads.submit(new ReadingNumbersFromFileTask(new MeanCallBack()));
        System.out.printf("Started tasks: %d\n",+startedTasks);
    }
    public void sortNumbers(){
        threads.submit(new ReadingNumbersFromFileTask(new SortCallBack()));
        System.out.printf("Started tasks: %d\n",+startedTasks);
    }

    public void finishedTask() {
        System.out.printf("Finished %d tasks\n",++finishedTasks);
    }

    @Override
    public void finishedTask(List<Integer> numbers) {
        System.out.println(numbers.toString());
        System.out.printf("Finished %d tasks\n",++finishedTasks);
    }
}
