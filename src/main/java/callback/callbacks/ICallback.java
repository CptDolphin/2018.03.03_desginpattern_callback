package callback.callbacks;

import java.util.List;

public interface ICallback {
    void finishedTask();
    void finishedTask(List<Integer> numbers);
}
