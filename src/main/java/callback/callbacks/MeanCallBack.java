package callback.callbacks;

import java.util.List;

public class MeanCallBack implements ICallback {
    @Override
    public void finishedTask() {
    }

    @Override
    public void finishedTask(List<Integer> result) {
        System.out.println("Task finished");
        System.out.println("Mean: " + result.stream().mapToInt(Integer::intValue).sum()/result.size());
    }
}
