package callback.callbacks;

import java.util.List;

public class SumCallBack implements ICallback {
    @Override
    public void finishedTask() {
    }

    @Override
    public void finishedTask(List<Integer> result) {
        System.out.println("Task finished");
        System.out.println("Sum: " + result.stream().mapToInt(Integer::intValue).sum());
    }
}
