package callback.callbacks;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Collections;
import java.util.List;

public class SortCallBack implements ICallback {
    @Override
    public void finishedTask() {
    }

    @Override
    public void finishedTask(List<Integer> numbers) {
        System.out.println("Task finished");
        sort(numbers);
        System.out.println("Sorted array: " + numbers.toString());
        try(PrintWriter writer = new PrintWriter("numbers.txt")){
            while (!numbers.isEmpty()){
                writer.print(numbers.remove(0));
                writer.print(" ");
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void sort(List<Integer> list) {
        sort(list, 0, list.size() - 1);
    }

    public static void sort(List<Integer> list, int from, int to) {
        if (from < to) {
            int pivot = from;
            int left = from + 1;
            int right = to;
            int pivotValue = list.get(pivot);
            while (left <= right) {
                while (left <= to && pivotValue >= list.get(left)) {
                    left++;
                }
                while (right > from && pivotValue < list.get(right)) {
                    right--;
                }
                if (left < right) {
                    Collections.swap(list, left, right);
                }
            }
            Collections.swap(list, pivot, left - 1);
            sort(list, from, right - 1);
            sort(list, right + 1, to);
        }
    }
}
